# Quản lý phần mềm

## Các trình quản lý phần mềm được cài sẳn trên VNux GNU/Linux

* `pacman` - Của [Arch Linux](https://archlinux.org/)
* `paru` - Phần mềm hỗ trợ cài đặt AUR, là wrapper cho `pacman`
* `pamac` - Của [Manjaro Linux](https://manjaro.org/), cung cấp giao diện để cài đặt phần mềm

## Pacman
* `pacman -S <Tên phần mềm>` - Để cài phần mềm nào đó
* `pacman -Ss <Tên phần mềm>` - Để tìm kiếm phần mềm nào đó
* `pacman -Sy` - Để đồng bộ cơ sở dữ liệu phần mềm (hữu ích nếu lỗi 404 xẩy ra)
* `pacman -Syy` - Như trên nhưng bắt buộc cập nhật lại toàn bộ repo (cho dù đang ở phiên bản mới nhất) (không nên dùng vì sẽ gây stress không cần thiết lên server)
* `pacman -Syu` - Để cập nhật hệ thống
